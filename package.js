Package.describe({
  summary: "EpicEditor"
});

Package.on_use(function (api, where) {
  var where = ['client'];

  api.add_files(['v0.2.2/js/epiceditor.js'], where);
  api.add_files(['v0.2.2/themes/base/epiceditor.css'], where);
  api.add_files(['v0.2.2/themes/editor/epic-light.css'], where);
  api.add_files(['v0.2.2/themes/preview/github.css'], where);
  
  api.export([
      'EpicEditor',
    ], 'client');
});
